import 'package:firebase_auth/firebase_auth.dart';

abstract class PhoneVerificationStates {}

class PhoneVerificationInitializedState extends PhoneVerificationStates {}

class PhoneVerificationCompletedState extends PhoneVerificationStates {
  final AuthCredential authCredential;

  PhoneVerificationCompletedState(this.authCredential);
}

class PhoneVerificationFailedState extends PhoneVerificationStates {
  final String message;

  PhoneVerificationFailedState(this.message);
}

class PhoneVerificationCodeSentState extends PhoneVerificationStates {
  final String verificationId;

  PhoneVerificationCodeSentState(this.verificationId);
}

class PhoneVerificationCodeAutoRetrievalTimeOutState
    extends PhoneVerificationStates {
  final String verificationId;

  PhoneVerificationCodeAutoRetrievalTimeOutState(this.verificationId);
}
