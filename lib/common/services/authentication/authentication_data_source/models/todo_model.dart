class TodoModel {
  String name;
  int date;
  int start;
  int end;
  List<String> category;
  bool remind;

  TodoModel({
    this.name,
    this.date,
    this.start,
    this.end,
    this.category,
    this.remind,
  });

  TodoModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    date = json['date'];
    start = json['start'];
    end = json['end'];
    category = json['category'].cast<String>();
    remind = json['remind'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['date'] = this.date;
    data['start'] = this.start;
    data['end'] = this.end;
    data['category'] = this.category;
    data['remind'] = this.remind;
    return data;
  }
}
