import 'package:firebase_auth/firebase_auth.dart';

class TodoUser {
  String id;
  String name;
  String email;

  TodoUser.fromFirebaseUser(FirebaseUser user) {
    this.id = user.uid;
    this.email = user.email;
    this.name = user.displayName;
  }

  @override
  String toString() {
    return "ID: $id, Name: $name, Email: $email";
  }
}
