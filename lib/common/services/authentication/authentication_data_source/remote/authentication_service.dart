import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:utodo/common/services/authentication/authentication_data_source/models/user_model.dart';
import 'package:utodo/core/models/failure.dart';

abstract class IAuthenticationService {
  Future<TodoUser> signUpWithEmail(String email, String password);
  Future<TodoUser> signInWithEmail(String email, String password);

  Future<TodoUser> isLogged();
}

class AuthenticationService implements IAuthenticationService {
  static final String tag = "AuthenticationService";

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  @override
  Future<TodoUser> signUpWithEmail(String email, String password) async {
    try {
      AuthResult authResult = await _firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password);

      return TodoUser.fromFirebaseUser(authResult.user);
    } on PlatformException catch (e) {
      throw Failure(message: e.message);
    } catch (e) {
      return Future.error(e.toString());
    }
  }

  @override
  Future<TodoUser> signInWithEmail(String email, String password) async {
    try {
      AuthResult authResult = await _firebaseAuth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );

      return TodoUser.fromFirebaseUser(authResult.user);
    } on PlatformException catch (e) {
      throw Failure(message: e.message);
    } catch (e) {
      return Future.error(e.toString());
    }
  }

  @override
  Future<TodoUser> isLogged() async {
    try {
      FirebaseUser user = await _firebaseAuth.currentUser();
      return TodoUser.fromFirebaseUser(user);
    } on PlatformException catch (e) {
      throw Failure(message: e.message);
    } catch (e) {
      return Future.error(e.toString());
    }
  }

  // final BehaviorSubject<PhoneVerificationStates> _phoneVerificationController =
  //     BehaviorSubject();

  // Stream<PhoneVerificationStates> get phoneVerificationObservable =>
  //     _phoneVerificationController.stream;

  // signUpWithPhoneNumber(String phoneNumber, String password) async {
  //   Log.d(tag, phoneNumber);

  //   _phoneVerificationController.add(PhoneVerificationInitializedState());

  //   _firebaseAuth.verifyPhoneNumber(
  //     phoneNumber: phoneNumber,
  //     timeout: Duration(seconds: 30),
  //     verificationCompleted: (AuthCredential auth) {
  //       _phoneVerificationController.add(
  //         PhoneVerificationCompletedState(auth),
  //       );
  //     },
  //     verificationFailed: (AuthException error) {
  //       _phoneVerificationController.add(
  //         PhoneVerificationFailedState(error.message),
  //       );
  //     },
  //     codeSent: (String verificationId, [int forceResendingToken]) {
  //       _phoneVerificationController.add(
  //         PhoneVerificationCodeSentState(verificationId),
  //       );
  //     },
  //     codeAutoRetrievalTimeout: (String verificationId) {
  //       _phoneVerificationController.add(
  //         PhoneVerificationCodeAutoRetrievalTimeOutState(verificationId),
  //       );
  //     },
  //   );
  // }

  // Future<bool> verifyPhoneNumber(String code, String verificationId) async {
  //   Log.d(tag, "Code $code \nVerification Id $verificationId");

  //   final AuthCredential _credential = PhoneAuthProvider.getCredential(
  //     verificationId: verificationId,
  //     smsCode: code,
  //   );

  //   try {
  //     AuthResult authResult = await FirebaseAuth.instance.signInWithCredential(
  //       _credential,
  //     );
  //     return true;
  //   } catch (e) {
  //     throw Failure(message: e.message);
  //   }
  // }

  // dispose() {
  //   _phoneVerificationController.close();
  // }

}
