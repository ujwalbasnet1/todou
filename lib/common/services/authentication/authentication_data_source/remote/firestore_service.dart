import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:utodo/common/services/authentication/authentication_data_source/models/todo_model.dart';
import 'package:utodo/core/models/failure.dart';

abstract class IFireStoreService {
  Future<String> addTodo(TodoModel todo);
}

class FireStoreService extends IFireStoreService {
  final CollectionReference _todosCollectionReference =
      Firestore.instance.collection('todos');


  @override
  Future<String> addTodo(TodoModel todo) async {
    try {
      var x = await _todosCollectionReference.add(todo.toJson());
      return x.documentID;
    } catch (e) {
      throw Failure(message: e.toString());
    }
  }
}
