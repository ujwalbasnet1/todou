import 'package:utodo/theme/colors.dart';
import 'package:flutter/material.dart';

class DChip extends StatefulWidget {
  final Function(bool) onChanged;
  final String text;

  DChip({Key key, this.onChanged, this.text}) : super(key: key);

  @override
  _DChipState createState() => _DChipState();
}

class _DChipState extends State<DChip> {
  bool isSelected = false;

  @override
  Widget build(BuildContext context) {
    return Ink(
      decoration: BoxDecoration(
        gradient: (isSelected == false) ? null : kGradientRose,
        borderRadius: BorderRadius.all(Radius.circular(24.0)),
        border: Border.all(color: kDisabledButtonColor.withOpacity(0.5)),
      ),
      child:  InkWell(
        borderRadius: BorderRadius.all(Radius.circular(24.0)),
        onTap: () {
          setState(() {
            isSelected= !isSelected;
          });
          if(widget.onChanged != null)
            widget.onChanged(isSelected);
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          child: Center(
            child: Text(
              widget.text ?? "",
              style: Theme.of(context).textTheme.subtitle,
            ),
          ),
        ),
      ),
    );
  }
}
