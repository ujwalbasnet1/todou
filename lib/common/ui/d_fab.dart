import 'package:utodo/theme/colors.dart';
import 'package:flutter/material.dart';

class DFab extends StatelessWidget {
  final Function onPressed;
  final IconData icon;

  const DFab({Key key, this.onPressed, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: onPressed,
      heroTag: null,
      elevation: 2,
      child: Ink(
        decoration: BoxDecoration(
          gradient: (onPressed == null) ? null : kGradientBlue,
          borderRadius: BorderRadius.all(Radius.circular(48.0)),
          color: (onPressed == null) ? kDisabledButtonColor : null,
        ),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Center(
            child: Icon(
              icon,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
