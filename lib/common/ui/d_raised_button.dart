import 'package:utodo/theme/colors.dart';
import 'package:flutter/material.dart';

class DRaisedButton extends StatelessWidget {
  final Function onPressed;
  final String text;
  final bool loading;
  final Color color;

  const DRaisedButton(
      {Key key, this.onPressed, this.text, this.loading = false, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      padding: EdgeInsets.all(0),
      onPressed: loading ? null : onPressed,
      elevation: 2,
      child: Ink(
        decoration: BoxDecoration(
          gradient: (color != null)
              ? null
              : ((onPressed == null) ? null : kGradientBlue),
          borderRadius: BorderRadius.all(Radius.circular(6.0)),
          color: color ?? ((onPressed == null) ? kDisabledButtonColor : null),
        ),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: 48,
          child: Center(
            child: loading
                ? Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Container(
                      width: 28,
                      height: 32,
                      child: CircularProgressIndicator(strokeWidth: 3),
                    ),
                  )
                : Text(
                    text?.toUpperCase() ?? "",
                    style: Theme.of(context)
                        .textTheme
                        .subtitle
                        .copyWith(fontWeight: FontWeight.w900),
                  ),
          ),
        ),
      ),
    );
  }
}
