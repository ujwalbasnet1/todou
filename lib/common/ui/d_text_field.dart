import 'package:flutter/material.dart';
import 'package:utodo/theme/colors.dart';

class DTextField extends StatelessWidget {
  final String hint;
  final TextEditingController controller;
  final bool password;
  final TextInputType textInputType;
  final int lines;
  final String prefixText;
  final bool enabled;

  DTextField({
    this.hint,
    this.controller,
    this.password = false,
    this.textInputType = TextInputType.text,
    this.lines = 1,
    this.prefixText,
    this.enabled = true,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      style: Theme.of(context).textTheme.subtitle,
      obscureText: password,
      keyboardType: textInputType,
      enabled: enabled,
      decoration: InputDecoration(
        enabled: enabled,
        hintText: hint,
        prefixText: prefixText,
        prefixStyle: Theme.of(context).textTheme.subtitle,
      ),
      maxLines: lines,
      minLines: lines,
    );
  }
}

class DUnderlineTextField extends StatelessWidget {
  final String hint;
  final TextEditingController controller;
  final bool password;
  final TextInputType textInputType;
  final int lines;
  final String prefixText;
  final IconData suffixIcon;
  final bool enabled;

  DUnderlineTextField({
    this.hint,
    this.controller,
    this.password = false,
    this.textInputType = TextInputType.text,
    this.lines = 1,
    this.prefixText,
    this.enabled = true,
    this.suffixIcon,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      style: Theme.of(context).textTheme.subtitle.copyWith(color: kMirageColor),
      obscureText: password,
      keyboardType: textInputType,
      enabled: enabled,
      decoration: InputDecoration(
        fillColor: Colors.transparent,
        contentPadding: EdgeInsets.symmetric(
          vertical: 0,
          horizontal: 0,
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(
            color: kMirageColor,
          ),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: kMirageColor,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: kMirageColor,
          ),
        ),
        enabled: enabled,
        hintText: hint,
        prefixText: prefixText,
        prefixStyle: Theme.of(context).textTheme.subtitle,
        suffix: (suffixIcon != null) ? Icon(suffixIcon) : null,
      ),
      maxLines: lines,
      minLines: lines,
    );
  }
}
