import 'package:utodo/theme/colors.dart';
import 'package:flutter/material.dart';

class DTipsContainer extends StatelessWidget {
  final String tips;

  const DTipsContainer({Key key, this.tips}) : assert(tips != null),super(key: key) ;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Column(
          children: <Widget>[
            SizedBox(height: 10,),
            Row(
              children: <Widget>[
                SizedBox(width: 8,),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      border: Border.all(color: kDisabledButtonColor.withOpacity(0.4)),
                      borderRadius: BorderRadius.circular(6),
                    ),
                    child: Text(tips, style: Theme.of(context).textTheme.caption.copyWith(color: kDisabledTextColor,),),
                  ),
                ),
              ],
            ),
          ],
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 4),
          decoration: BoxDecoration(gradient: kGradientRose, borderRadius: BorderRadius.only(
            topRight: Radius.circular(2),
            bottomLeft: Radius.circular(6),
            bottomRight: Radius.circular(6),
            topLeft: Radius.circular(6),
          )),
          child: Text('Tips', style: Theme.of(context).textTheme.subhead.copyWith(fontSize: 10),),
        ),
      ],
    );
  }
}
