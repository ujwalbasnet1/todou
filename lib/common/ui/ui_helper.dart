import 'package:flutter/widgets.dart';

// --- vertical spacing ---
final esHeightSpan = const SizedBox(height: 4);
final sHeightSpan = const SizedBox(height: 10);
final mHeightSpan = const SizedBox(height: 16);
final lHeightSpan = const SizedBox(height: 24);
final elHeightSpan = const SizedBox(height: 48);

// --- horizontal spacing ---
final esWidthSpan = const SizedBox(width: 4);
final sWidthSpan = const SizedBox(width: 10);
final mWidthSpan = const SizedBox(width: 16);
final elWidthSpan = const SizedBox(width: 24);

// --- page paddings ----
final mPagePadding = const EdgeInsets.symmetric(horizontal: 32);
final sPagePadding = const EdgeInsets.symmetric(horizontal: 16);
