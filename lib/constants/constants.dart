final String kAccountImage = 'assets/images/account.png';
final String kNameImage = 'assets/images/name.png';
final String kAboutMeImage = 'assets/images/about_me.png';
final String kGenderImage = 'assets/images/gender.png';
final String kSelectMaleImage = 'assets/images/select_male.png';
final String kSelectFemaleImage = 'assets/images/select_female.png';
final String kBirthdayImage = 'assets/images/birthday.png';
final String kMyInterestImage = 'assets/images/interest.png';

final String appName = "UTodo";
