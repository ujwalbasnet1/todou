import 'package:flutter/material.dart';


class NavigationService {
  final GlobalKey<NavigatorState> _navigationKey = GlobalKey<NavigatorState>();

  GlobalKey<NavigatorState> get navigationKey => _navigationKey;

  bool pop() {
    return _navigationKey.currentState.pop();
  }

  Future<dynamic> navigateToRoute(String routeName,
      {dynamic arguments, bool clearStack = false}) {
    return _navigationKey.currentState.pushNamedAndRemoveUntil(
      routeName,
      (_) => !clearStack,
      arguments: arguments,
    );
  }

  Future<dynamic> navigateTo(Widget widget, {bool clearStack = false}) {
    if (clearStack) {
      return _navigationKey.currentState
          .pushAndRemoveUntil(FadePageTransition(child: widget), (_) => false);
    }

    return _navigationKey.currentState.push(FadePageTransition(child: widget));
  }

  NavigationService._();

  factory NavigationService() {
    return _instance;
  }

  static final _instance = NavigationService._();
}



class FadePageTransition<T> extends PageRouteBuilder<T> {
  final Widget child;
  final Curve curve;
  final Alignment alignment;
  final Duration duration;

  FadePageTransition({
    Key key,
    @required this.child,
    this.curve = Curves.linear,
    this.alignment,
    this.duration = const Duration(milliseconds: 300),
    RouteSettings settings,
  }) : super(
    pageBuilder: (BuildContext context, Animation<double> animation,
        Animation<double> secondaryAnimation) {
      return child;
    },
    transitionDuration: duration,
    settings: settings,
    transitionsBuilder: (BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        Widget child) {
      return FadeTransition(opacity: animation, child: child);
    },
  );
}
