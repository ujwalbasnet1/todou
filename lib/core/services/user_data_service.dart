import 'package:utodo/common/services/authentication/authentication_data_source/models/user_model.dart';
import 'package:utodo/core/logger/logger.dart';

final String tokenKey = "TOKEN";
final String nameKey = "NAME";

class UserDataService {
  static const String tag = "UserDataService";

  TodoUser _user;

  TodoUser get user => _user;

  set setUser(TodoUser user) {
    _user = user;
    Log.d(tag, "User For Reference Saved");
  }

  UserDataService._();

  factory UserDataService() {
    return _instance;
  }

  static final _instance = UserDataService._();

  // String _token;
  // String _name;
  // String verificationId;

  // String get token => _token;

  // String get name => _name;

  // bool saveData(String token, String name) {
  //   _token = token;
  //   _name = name;
  //   SharedPreferencesManager.preferences.setString(tokenKey, token);
  //   SharedPreferencesManager.preferences.setString(nameKey, name);

  //   Log.d(TAG, "Data Saved");
  //   return true;
  // }

  // bool getData() {
  //   _token = SharedPreferencesManager.preferences.getString(tokenKey);
  //   _name = SharedPreferencesManager.preferences.getString(nameKey);
  //   Log.d(TAG, "Data Retrieved");
  //   return true;
  // }

  // bool clearData() {
  //   _token = null;
  //   _name = null;
  //   SharedPreferencesManager.preferences.remove(tokenKey);
  //   SharedPreferencesManager.preferences.remove(nameKey);
  //   Log.d(TAG, "Data Cleared");
  //   return true;
  // }

  // bool get isLoggedIn {
  //   getData();
  //   if (_token != null &&
  //       _token.length > 0 &&
  //       _name != null &&
  //       _name.length > 0) {
  //     Log.d(TAG, "Logged In");
  //     return true;
  //   }
  //   Log.d(TAG, "Not Logged In");
  //   return false;
  // }

}
