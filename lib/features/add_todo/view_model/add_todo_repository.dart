import 'package:utodo/common/services/authentication/authentication_data_source/models/todo_model.dart';
import 'package:utodo/common/services/authentication/authentication_data_source/remote/firestore_service.dart';

abstract class IAddTodoRepository {
  Future<String> addTodo(TodoModel todo);
}

class AddTodoRepository implements IAddTodoRepository {
  final IFireStoreService fireStoreService;

  AddTodoRepository(this.fireStoreService);

  @override
  Future<String> addTodo(TodoModel todo) {
    return fireStoreService.addTodo(todo);
  }
}
