import 'package:utodo/common/services/authentication/authentication_data_source/models/todo_model.dart';
import 'package:utodo/core/models/base_view_model.dart';
import 'package:utodo/core/services/dialog_service.dart';
import 'package:utodo/core/services/navigation_service.dart';
import 'package:utodo/features/add_todo/view_model/add_todo_repository.dart';
import 'package:utodo/locator.dart';

import 'package:utodo/core/models/failure.dart';

class AddTodoViewModel extends BaseViewModel {
  static final String tag = "AddTodoViewModel";

  DialogService _dialogService = locator<DialogService>();
  NavigationService _navigationService = locator<NavigationService>();

  IAddTodoRepository _addTodoRepository = locator<IAddTodoRepository>();

  void addTodo(TodoModel todo) async {
    try {
      setBusy(true);

      await _addTodoRepository.addTodo(todo);
      _navigationService.pop();

      setBusy(false);
    } on Failure catch (e) {
      _dialogService.showDialog(
        title: "Add Todo Failure",
        description: e.message,
        buttonTitle: "Ok",
      );
    }
    setBusy(false);
  }
}
