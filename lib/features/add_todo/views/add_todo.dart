import 'package:flutter/material.dart';
import 'package:utodo/common/ui/d_raised_button.dart';
import 'package:utodo/common/ui/d_text_field.dart';
import 'package:utodo/common/ui/ui_helper.dart';
import 'package:utodo/features/add_todo/widgets/category_select_field_widget.dart';
import 'package:utodo/features/add_todo/widgets/select_field_widget.dart';
import 'package:utodo/theme/colors.dart';

class AddTodoView extends StatefulWidget {
  @override
  _AddTodoViewState createState() => _AddTodoViewState();
}

class _AddTodoViewState extends State<AddTodoView> {
  int currentIndex = 0; // 0 being default page
  TextEditingController _nameController;

  DateTime startDate;
  TimeOfDay startTime;

  DateTime endDate;
  TimeOfDay endTime;

  bool remind = true;

  @override
  void initState() {
    _nameController = TextEditingController();

    startTime = TimeOfDay.now();
    startDate = DateTime.now();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFFffd7c2),
      resizeToAvoidBottomInset: false,
      appBar: AppBar(),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            _topSection(),
            lHeightSpan,
            Expanded(
              child: Container(
                padding: mPagePadding,
                decoration: BoxDecoration(
                  color: Color(0XFFeeeeee),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(24),
                    topLeft: Radius.circular(24),
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    lHeightSpan,
                    sHeightSpan,
                    Text(
                      "From",
                      style: Theme.of(context)
                          .textTheme
                          .caption
                          .copyWith(color: kMirageColor.withOpacity(0.5)),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Theme(
                            data: ThemeData.light(),
                            child: Builder(builder: (context) {
                              return SelectFieldWidget(
                                text: "${startTime.hour}:${startTime.minute}",
                                icon: Icons.timer,
                                onPressed: () async {
                                  TimeOfDay picked = await showTimePicker(
                                    context: context,
                                    initialTime: TimeOfDay.now(),
                                  );

                                  if (picked != null) {
                                    setState(() {
                                      startTime = picked;
                                    });
                                  }
                                },
                              );
                            }),
                          ),
                        ),
                        sWidthSpan,
                        Expanded(
                          flex: 3,
                          child: Theme(
                            data: ThemeData.light(),
                            child: Builder(builder: (context) {
                              return SelectFieldWidget(
                                text:
                                    "${startDate.day}/${startDate.month}/${startDate.year}",
                                icon: Icons.calendar_today,
                                onPressed: () async {
                                  DateTime picked = await showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime(1990, 1, 1),
                                      lastDate: DateTime.now()
                                          .add(Duration(days: 365)));

                                  if (picked != null) {
                                    setState(() {
                                      startDate = picked;
                                    });
                                  }
                                },
                              );
                            }),
                          ),
                        ),
                      ],
                    ),
                    mHeightSpan,
                    Text(
                      "To",
                      style: Theme.of(context)
                          .textTheme
                          .caption
                          .copyWith(color: kMirageColor.withOpacity(0.5)),
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Theme(
                            data: ThemeData.light(),
                            child: Builder(builder: (context) {
                              return SelectFieldWidget(
                                text: "8:00",
                                icon: Icons.timer,
                                onPressed: () async {
                                  TimeOfDay picked = await showTimePicker(
                                    context: context,
                                    initialTime: TimeOfDay.now(),
                                  );

                                  if (picked != null) {
                                    // picked time
                                  }
                                },
                              );
                            }),
                          ),
                        ),
                        sWidthSpan,
                        Expanded(
                          flex: 3,
                          child: Theme(
                            data: ThemeData.light(),
                            child: Builder(builder: (context) {
                              return SelectFieldWidget(
                                text: "Feb 24, Monday",
                                icon: Icons.calendar_today,
                                onPressed: () async {
                                  TimeOfDay picked = await showTimePicker(
                                    context: context,
                                    initialTime: TimeOfDay.now(),
                                  );

                                  if (picked != null) {
                                    // picked time
                                  }
                                },
                              );
                            }),
                          ),
                        ),
                      ],
                    ),
                    mHeightSpan,
                    _remindMeSection(),
                    mHeightSpan,
                    _categorySection(),
                    lHeightSpan,
                    DRaisedButton(
                      color: kMirageColor,
                      text: "Create Task",
                      onPressed: () {},
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _categorySection() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Category",
            style: Theme.of(context)
                .textTheme
                .caption
                .copyWith(color: kMirageColor.withOpacity(0.5)),
          ),
          sHeightSpan,
          Wrap(
            spacing: 6,
            runSpacing: 6,
            children: <Widget>[
              SelectCategoryField(categoryName: "Food"),
              SelectCategoryField(categoryName: "Health"),
              SelectCategoryField(categoryName: "Education"),
              SelectCategoryField(categoryName: "Dance"),
            ],
          ),
          sHeightSpan,
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.orangeAccent.withOpacity(0.2),
                borderRadius: BorderRadius.circular(16),
              ),
              padding: const EdgeInsets.all(4),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  CircleAvatar(
                    backgroundColor: Colors.orangeAccent,
                    radius: 10,
                    child: Icon(Icons.add, size: 12, color: Colors.white),
                  ),
                  Text(
                    " Add Tag   ",
                    style: Theme.of(context).textTheme.body1.copyWith(
                          color: Colors.orangeAccent,
                        ),
                  ),
                ],
              ),
            ),
          ),
        ],
      );

  _remindMeSection() => Row(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: kMirageColor.withOpacity(0.15),
            ),
            padding: const EdgeInsets.all(8),
            child: Icon(
              Icons.notifications_none,
              color: kMirageColor,
              size: 16,
            ),
          ),
          sWidthSpan,
          Text(
            "Remind Me",
            style: Theme.of(context)
                .textTheme
                .subtitle
                .copyWith(color: kMirageColor),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.centerRight,
              child: Switch(
                value: remind,
                onChanged: (val) => setState(() => remind = val),
                activeColor: kMirageColor,
              ),
            ),
          ),
        ],
      );

  _topSection() => Padding(
        padding: sPagePadding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "Create\nNew Task",
              style: Theme.of(context)
                  .textTheme
                  .display3
                  .copyWith(color: kMirageColor),
            ),
            lHeightSpan,
            Text(
              "Name",
              style: Theme.of(context)
                  .textTheme
                  .subtitle
                  .copyWith(color: kMirageColor),
            ),
            DUnderlineTextField(
              hint: "Task name",
              controller: _nameController,
            ),
          ],
        ),
      );
}
