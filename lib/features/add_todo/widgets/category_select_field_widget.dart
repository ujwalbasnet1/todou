import 'dart:math';

import 'package:flutter/material.dart';
import 'package:utodo/features/home/widgets/long_todo_item_widget.dart';

class SelectCategoryField extends StatelessWidget {
  final String categoryName;

  const SelectCategoryField({Key key, this.categoryName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Color color = colors[Random().nextInt(colors.length)];

    return Container(
      decoration: BoxDecoration(
        color: color.withOpacity(0.1),
        borderRadius: BorderRadius.circular(4),
      ),
      padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 16),
      child: Text(
        categoryName,
        style: Theme.of(context).textTheme.body1.copyWith(
              color: color,
            ),
      ),
    );
  }
}
