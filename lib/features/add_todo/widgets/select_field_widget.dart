import 'package:flutter/material.dart';
import 'package:utodo/common/ui/ui_helper.dart';
import 'package:utodo/theme/colors.dart';

class SelectFieldWidget extends StatelessWidget {
  final String text;
  final IconData icon;
  final String label;
  final Function onPressed;

  const SelectFieldWidget(
      {Key key, this.text = "", this.icon, this.label, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        color: Colors.transparent,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            (label == null)
                ? Container()
                : Text(
                    label,
                    style: Theme.of(context)
                        .textTheme
                        .caption
                        .copyWith(color: kMirageColor.withOpacity(0.5)),
                  ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(
                  text,
                  style: Theme.of(context)
                      .textTheme
                      .subtitle
                      .copyWith(color: Colors.black),
                ),
                Icon(icon, color: kMirageColor, size: 18),
              ],
            ),
            sHeightSpan,
            Container(
              height: 1,
              color: kMirageColor,
            ),
          ],
        ),
      ),
    );
  }
}
