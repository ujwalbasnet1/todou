import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:utodo/common/ui/ui_helper.dart';
import 'package:utodo/core/services/navigation_service.dart';
import 'package:utodo/features/add_todo/views/add_todo.dart';
import 'package:utodo/features/home/view_model/home_view_model.dart';
import 'package:utodo/features/home/widgets/bottom_nav_bar.dart';
import 'package:utodo/features/home/widgets/long_todo_item_widget.dart';
import 'package:utodo/features/home/widgets/todo_item_widget.dart';
import 'package:utodo/locator.dart';
import 'package:utodo/theme/colors.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  int currentIndex = 0; // 0 being default page

  final NavigationService _navigationService = locator<NavigationService>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFFfaf3f0),
      bottomNavigationBar: _buttonNavigationBar(),
      body: SafeArea(
        child: ViewModelProvider<HomeViewModel>.withConsumer(
          viewModel: HomeViewModel(),
          builder: (context, model, child) {
            return ListView(
              padding: sPagePadding,
              physics: BouncingScrollPhysics(),
              children: <Widget>[
                lHeightSpan,
                _todaySection(),
                lHeightSpan,
                _tomorrowSection(),
                lHeightSpan,
              ],
            );
          },
        ),
      ),
    );
  }

  _todaySection() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Today",
            style: Theme.of(context)
                .textTheme
                .display4
                .copyWith(color: Colors.black),
          ),
          mHeightSpan,
          ListView.separated(
            itemCount: 5,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) => LongTodoItemWidget(
              icon: Icons.fastfood,
              title: "Eating Lunch",
              date: "Feb 25",
              from: "8:00",
              to: "9:00",
            ),
            separatorBuilder: (context, index) => sHeightSpan,
          ),
        ],
      );

  _tomorrowSection() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            "Tomorrow",
            style: Theme.of(context)
                .textTheme
                .display4
                .copyWith(color: Colors.black),
          ),
          mHeightSpan,
          SizedBox(
            height: 148,
            child: ListView.separated(
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemCount: 5,
              separatorBuilder: (context, index) => sWidthSpan,
              itemBuilder: (context, index) => TodoItemWidget(
                icon: Icons.fastfood,
                title: "Eating Lunch",
                date: "Feb 25",
                from: "8:00",
                to: "9:00",
              ),
            ),
          ),
        ],
      );

  _buttonNavigationBar() => BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: (int selectedIndex) {
          setState(() {
            currentIndex = selectedIndex;
          });

          switch (selectedIndex) {
            case 2:
              _navigationService.navigateTo(AddTodoView());
              break;
            default:
          }
        },
        selectedItemColor: kMirageColor,
        unselectedItemColor: kMirageColor.withOpacity(0.25),
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        items: bottomNavigationBarItems,
      );
}
