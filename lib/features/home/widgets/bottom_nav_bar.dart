import 'package:flutter/material.dart';
import 'package:utodo/theme/colors.dart';

List<BottomNavigationBarItem> bottomNavigationBarItems = [
  BottomNavigationBarItem(
    icon: Icon(Icons.dashboard),
    title: SizedBox.shrink(),
  ),
  BottomNavigationBarItem(
    icon: Icon(Icons.timer),
    title: SizedBox.shrink(),
  ),
  BottomNavigationBarItem(
    icon: Container(
      width: 48,
      height: 48,
      child: Center(
        child: CircleAvatar(
          radius: 24,
          child: Icon(Icons.add, color: Colors.white),
          backgroundColor: kMirageColor,
        ),
      ),
    ),
    title: SizedBox.shrink(),
  ),
  BottomNavigationBarItem(
    icon: Icon(Icons.check),
    title: SizedBox.shrink(),
  ),
  BottomNavigationBarItem(
    icon: Icon(Icons.person_outline),
    title: SizedBox.shrink(),
  ),
];
