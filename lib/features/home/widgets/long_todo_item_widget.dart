import 'dart:math';

import 'package:flutter/material.dart';
import 'package:utodo/common/ui/ui_helper.dart';
import 'package:utodo/theme/colors.dart';

List<Color> colors = const [
  Colors.redAccent,
  Colors.pinkAccent,
  Colors.purpleAccent,
  Colors.deepPurpleAccent,
  Colors.indigoAccent,
  Colors.blueAccent,
  Colors.lightBlueAccent,
  Colors.greenAccent,
  Colors.orangeAccent,
  Colors.deepOrangeAccent
];

class LongTodoItemWidget extends StatelessWidget {
  final IconData icon;
  final String title;
  final String date;
  final String from;
  final String to;

  const LongTodoItemWidget({
    Key key,
    @required this.icon,
    @required this.title,
    @required this.date,
    @required this.from,
    @required this.to,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Color iconColor = colors[Random().nextInt(colors.length)];

    return Container(
      padding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              CircleAvatar(
                radius: 24,
                backgroundColor: iconColor.withOpacity(0.1),
                child: Icon(
                  icon,
                  color: iconColor,
                ),
              ),
              mWidthSpan,
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "$title",
                    style: Theme.of(context)
                        .textTheme
                        .title
                        .copyWith(color: Colors.black),
                  ),
                  esHeightSpan,
                  Text(
                    "$date, $from - $to",
                    style: Theme.of(context)
                        .textTheme
                        .caption
                        .copyWith(color: Colors.black45),
                  ),
                ],
              )
            ],
          ),
          Icon(Icons.more_horiz, color: kMirageColor),
        ],
      ),
    );
  }
}
