import 'dart:math';

import 'package:flutter/material.dart';
import 'package:utodo/common/ui/ui_helper.dart';

import 'long_todo_item_widget.dart';

class TodoItemWidget extends StatelessWidget {
  final IconData icon;
  final String title;
  final String date;
  final String from;
  final String to;

  const TodoItemWidget({
    Key key,
    @required this.icon,
    @required this.title,
    @required this.date,
    @required this.from,
    @required this.to,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Color iconColor = colors[Random().nextInt(colors.length)];

    return Container(
      width: 148,
      height: 152,
      padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CircleAvatar(
            radius: 24,
            backgroundColor: iconColor.withOpacity(0.1),
            child: Icon(
              icon,
              color: iconColor,
            ),
          ),
          mHeightSpan,
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "$date, $from - $to",
                style: Theme.of(context)
                    .textTheme
                    .caption
                    .copyWith(color: Colors.black45),
              ),
              esHeightSpan,
              Text(
                "$title",
                maxLines: 1,
                overflow: TextOverflow.clip,
                style: Theme.of(context).textTheme.subtitle.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                    ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
