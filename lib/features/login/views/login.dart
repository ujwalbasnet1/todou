import 'dart:ui';

import 'package:provider_architecture/viewmodel_provider.dart';
import 'package:utodo/common/ui/d_raised_button.dart';
import 'package:utodo/common/ui/d_text_field.dart';
import 'package:utodo/common/ui/ui_helper.dart';
import 'package:utodo/constants/constants.dart';
import 'package:utodo/core/services/navigation_service.dart';
import 'package:utodo/features/login/view_model/login_view_model.dart';
import 'package:utodo/features/signup/views/signup.dart';
import 'package:utodo/locator.dart';
import 'package:utodo/theme/colors.dart';
import 'package:flutter/material.dart';

class LogInView extends StatefulWidget {
  @override
  _LogInViewState createState() => _LogInViewState();
}

class _LogInViewState extends State<LogInView> {
  final NavigationService _navigationService = locator<NavigationService>();

  TextEditingController _emailController, _passwordController;

  @override
  void initState() {
    _emailController = TextEditingController();
    _passwordController = TextEditingController();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Column(
            children: <Widget>[
              sHeightSpan,
              Image.asset(
                kAccountImage,
                width: MediaQuery.of(context).size.width,
              ),
              Padding(
                padding: mPagePadding,
                child: ViewModelProvider<LoginViewModel>.withConsumer(
                    viewModel: LoginViewModel(),
                    builder: (context, model, child) {
                      return Form(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.05,
                            ),
                            Text(
                              'Please sign in to continue.',
                              style: Theme.of(context).textTheme.display4,
                            ),
                            sHeightSpan,
                            sHeightSpan,
                            DTextField(
                              textInputType: TextInputType.emailAddress,
                              hint: 'Email',
                              controller: _emailController,
                              enabled: !model.busy,
                            ),
                            sHeightSpan,
                            DTextField(
                              password: true,
                              hint: 'Password',
                              controller: _passwordController,
                              enabled: !model.busy,
                            ),
                            sHeightSpan,
                            sHeightSpan,
                            sHeightSpan,
                            Container(
                              child: DRaisedButton(
                                text: 'Sign In',
                                onPressed: () {
                                  FocusScope.of(context).requestFocus(
                                    new FocusNode(),
                                  );

                                  model.signIn(
                                    email: _emailController.text,
                                    password: _passwordController.text,
                                  );
                                },
                              ),
                            ),
                            sHeightSpan,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Don't have an account? ",
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle
                                      .copyWith(
                                          color: kDisabledTextColor,
                                          fontSize: 12),
                                ),
                                GestureDetector(
                                  onTap: model.busy
                                      ? null
                                      : () {
                                          _navigationService.navigateTo(
                                            SignUpView(),
                                            clearStack: true,
                                          );
                                        },
                                  child: Text(
                                    'Sign Up',
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle
                                        .copyWith(
                                            color: kDisabledTextColor,
                                            fontSize: 12,
                                            fontWeight: FontWeight.w900),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      );
                    }),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
