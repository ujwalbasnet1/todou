import 'package:utodo/common/services/authentication/authentication_data_source/models/user_model.dart';
import 'package:utodo/common/services/authentication/authentication_data_source/remote/authentication_service.dart';
import 'package:utodo/core/models/failure.dart';

abstract class ISignUpRepository {
  Future<TodoUser> signUp(String email, String password);
  Future<TodoUser> signIn(String email, String password);

  // Future<bool> verify(String code, String verificationId);
}

class SignUpRepository implements ISignUpRepository {
  final IAuthenticationService authenticationService;

  SignUpRepository(this.authenticationService);

  @override
  Future<TodoUser> signUp(String email, String password) async {
    if (password.length < 6) {
      throw Failure(message: 'Password must be longer than 6 characters');
    }

    return authenticationService.signUpWithEmail(email, password);
  }

  @override
  Future<TodoUser> signIn(String email, String password) {
    return authenticationService.signInWithEmail(email, password);
  }

  // @override
  // Future<bool> verify(String code, String verificationId) async {
  //   return await authenticationService.verifyPhoneNumber(code, verificationId);
  // }
}
