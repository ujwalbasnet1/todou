import 'package:flutter/foundation.dart';
import 'package:utodo/core/logger/logger.dart';
import 'package:utodo/core/models/base_view_model.dart';
import 'package:utodo/core/models/failure.dart';
import 'package:utodo/core/services/dialog_service.dart';
import 'package:utodo/core/services/navigation_service.dart';
import 'package:utodo/core/services/user_data_service.dart';
import 'package:utodo/features/home/views/home.dart';
import 'package:utodo/features/signup/view_model/signup_repository.dart';
import 'package:utodo/locator.dart';

class SignUpViewModel extends BaseViewModel {
  static final String tag = "SignUpViewModel";

  DialogService _dialogService = locator<DialogService>();
  NavigationService _navigationService = locator<NavigationService>();
  ISignUpRepository _signUpRepository = locator<ISignUpRepository>();

  void signUp({
    @required String email,
    @required String password,
    @required String confirmPassword,
  }) async {
    try {
      if (email.isEmpty) {
        throw Failure(message: "Please enter a valid email");
      }

      if (password.isEmpty) {
        throw Failure(message: "Please enter a valid password");
      }

      if (password.compareTo(confirmPassword) != 0) {
        throw Failure(message: "Password doesn't match");
      }

      setBusy(true);

      var user = await _signUpRepository.signUp(email, password);
      Log.d(tag, user);
      UserDataService().setUser = user;

      if (user != null) {
        _navigationService.navigateTo(HomeView(), clearStack: true);
      }
    } on Failure catch (e) {
      _dialogService.showDialog(
        title: "Sign Up Failure",
        description: e.message,
        buttonTitle: "Ok",
      );

      setBusy(false);
    }
  }

  // void verify({@required String code}) async {
  //   try {
  //     bool verified = await _signUpRepository.verify(
  //         code, UserDataService().verificationId);

  //     if (verified) {
  //       // D-TODO: Create new user
  //       Log.d(tag, "LOGGED_IN");
  //     }
  //   } on Failure catch (e) {
  //     _dialogService.showDialog(
  //       title: "Sign Up Failure",
  //       description: e.message,
  //       buttonTitle: "Ok",
  //     );
  //   }
  // }
}
