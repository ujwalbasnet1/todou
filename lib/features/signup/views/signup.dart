import 'dart:ui';

import 'package:provider_architecture/provider_architecture.dart';
import 'package:utodo/common/ui/d_raised_button.dart';
import 'package:utodo/common/ui/d_text_field.dart';
import 'package:utodo/common/ui/ui_helper.dart';
import 'package:utodo/constants/constants.dart';
import 'package:utodo/core/services/navigation_service.dart';
import 'package:utodo/features/login/views/login.dart';
import 'package:utodo/features/signup/view_model/signup_view_model.dart';
import 'package:utodo/locator.dart';
import 'package:utodo/theme/colors.dart';
import 'package:flutter/material.dart';

class SignUpView extends StatefulWidget {
  @override
  _SignUpViewState createState() => _SignUpViewState();
}

class _SignUpViewState extends State<SignUpView> {
  final NavigationService _navigationService = locator<NavigationService>();

  TextEditingController _emailController,
      _passwordController,
      _confirmPasswordController;

  @override
  void initState() {
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    _confirmPasswordController = TextEditingController();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Column(
            children: <Widget>[
              sHeightSpan,
              Image.asset(
                kAccountImage,
                width: MediaQuery.of(context).size.width,
              ),
              Padding(
                padding: mPagePadding,
                child: ViewModelProvider<SignUpViewModel>.withConsumer(
                    viewModel: SignUpViewModel(),
                    builder: (context, model, child) {
                      return Form(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.05,
                            ),
                            Text(
                              'Create account to continue.',
                              style: Theme.of(context).textTheme.display4,
                            ),
                            sHeightSpan,
                            sHeightSpan,
                            DTextField(
                              textInputType: TextInputType.emailAddress,
                              hint: 'Email',
                              controller: _emailController,
                              enabled: !model.busy,
                            ),
                            sHeightSpan,
                            DTextField(
                              password: true,
                              hint: 'Password',
                              controller: _passwordController,
                              enabled: !model.busy,
                            ),
                            sHeightSpan,
                            DTextField(
                              password: true,
                              hint: 'Confirm Password',
                              controller: _confirmPasswordController,
                              enabled: !model.busy,
                            ),
                            sHeightSpan,
                            sHeightSpan,
                            sHeightSpan,
                            Container(
                              child: DRaisedButton(
                                text: 'Register',
                                loading: model.busy,
                                onPressed: () {
                                  FocusScope.of(context).requestFocus(
                                    new FocusNode(),
                                  );

                                  model.signUp(
                                    email: _emailController.text,
                                    password: _passwordController.text,
                                    confirmPassword:
                                        _confirmPasswordController.text,
                                  );
                                },
                              ),
                            ),
                            sHeightSpan,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Already have an account? ",
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle
                                      .copyWith(
                                          color: kDisabledTextColor,
                                          fontSize: 12),
                                ),
                                GestureDetector(
                                  onTap: model.busy
                                      ? null
                                      : () {
                                          _navigationService.navigateTo(
                                            LogInView(),
                                            clearStack: true,
                                          );
                                        },
                                  child: Text(
                                    'Sign In',
                                    style: Theme.of(context)
                                        .textTheme
                                        .subtitle
                                        .copyWith(
                                            color: kDisabledTextColor,
                                            fontSize: 12,
                                            fontWeight: FontWeight.w900),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      );
                    }),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
