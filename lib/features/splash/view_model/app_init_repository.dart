import 'package:utodo/common/services/authentication/authentication_data_source/models/user_model.dart';
import 'package:utodo/common/services/authentication/authentication_data_source/remote/authentication_service.dart';

abstract class IAppInitRepository {
  Future<TodoUser> isLogged();
}

class AppInitRepository extends IAppInitRepository {
  final IAuthenticationService authenticationService;

  AppInitRepository(this.authenticationService);

  @override
  Future<TodoUser> isLogged() {
    return authenticationService.isLogged();
  }
}
