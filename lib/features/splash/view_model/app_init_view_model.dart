import 'package:utodo/common/services/authentication/authentication_data_source/models/user_model.dart';
import 'package:utodo/core/logger/logger.dart';
import 'package:utodo/core/models/base_view_model.dart';
import 'package:utodo/core/models/failure.dart';
import 'package:utodo/core/services/navigation_service.dart';
import 'package:utodo/core/services/user_data_service.dart';
import 'package:utodo/features/home/views/home.dart';
import 'package:utodo/features/login/views/login.dart';
import 'package:utodo/features/splash/view_model/app_init_repository.dart';
import 'package:utodo/locator.dart';

class AppInitViewModel extends BaseViewModel {
  static final String tag = "AppInitViewModel";

  NavigationService _navigationService = locator<NavigationService>();
  IAppInitRepository _appInitRepository = locator<IAppInitRepository>();

  void isLogged() async {
    try {
      setBusy(true);

      TodoUser user = await _appInitRepository.isLogged();
      await Future.delayed(Duration(milliseconds: 900));
      Log.d(tag, user);
      UserDataService().setUser = user;

      if (user != null) {
        _navigationService.navigateTo(HomeView(), clearStack: true);
      } else {
        _navigationService.navigateTo(LogInView(), clearStack: true);
      }

      setBusy(false);
    } on Failure {
      _navigationService.navigateTo(LogInView(), clearStack: true);
      setBusy(false);
    }
  }
}
