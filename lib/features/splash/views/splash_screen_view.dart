import 'package:flutter/material.dart';
import 'package:provider_architecture/provider_architecture.dart';
import 'package:utodo/features/splash/view_model/app_init_view_model.dart';

class SplashScreenView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ViewModelProvider<AppInitViewModel>.withConsumer(
          viewModel: AppInitViewModel(),
          onModelReady: (model) => model.isLogged(),
          staticChild: Center(child: Icon(Icons.work, size: 120)),
          builder: (context, model, child) {
            return child;
          }),
    );
  }
}
