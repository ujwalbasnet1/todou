import 'package:get_it/get_it.dart';

import 'common/services/authentication/authentication_data_source/remote/authentication_service.dart';
import 'core/services/dialog_service.dart';
import 'core/services/navigation_service.dart';
import 'features/signup/view_model/signup_repository.dart';
import 'features/splash/view_model/app_init_repository.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
// core dependencies

  // http service
//  locator.registerLazySingleton<HttpService>(() => HttpService());

  // navigation dependency
  locator.registerLazySingleton<NavigationService>(() => NavigationService());
  // add dialog dependency
  locator.registerLazySingleton<DialogService>(() => DialogService());

  // services
  locator.registerLazySingleton<IAuthenticationService>(
    () => AuthenticationService(),
  );
  // features dependencies
  _initAppInitDependency();
  _initAuthenticationDependency();
}

/// to add new feature dependency create a _setup<feature-name> function and add call in setupLocator

_initAppInitDependency() {
// repository

  locator.registerLazySingleton<IAppInitRepository>(
    () => AppInitRepository(locator()),
  );
}

_initAuthenticationDependency() {
// repository

  locator.registerLazySingleton<ISignUpRepository>(
    () => SignUpRepository(locator()),
  );
}
