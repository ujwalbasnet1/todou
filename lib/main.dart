import 'package:flutter/material.dart';
import 'package:utodo/theme/theme.dart';

import 'constants/constants.dart';
import 'core/managers/dialog_managers.dart';
import 'core/services/dialog_service.dart';
import 'core/services/navigation_service.dart';
import 'features/splash/views/splash_screen_view.dart';
import 'locator.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // note: not intialized as firebase saves data for local usage
  // SharedPreferencesManager.preferences = await SharedPreferences.getInstance();

  setupLocator();

  // note: not initialized as we use firebase which handles this
  // UserDataService().getData();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appName,
      theme: kDatingTheme,
      builder: (context, child) => Navigator(
        key: locator<DialogService>().dialogNavigationKey,
        onGenerateRoute: (settings) => MaterialPageRoute(
          builder: (context) => DialogManager(
            child: child,
          ),
        ),
      ),
      navigatorKey: locator<NavigationService>().navigationKey,
      home: SplashScreenView(),
    );
  }
}
