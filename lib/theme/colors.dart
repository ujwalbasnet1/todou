import 'package:flutter/material.dart';

Color kMirageColor = Color(0XFF201932);
Color kPurpleColor = Color(0XFF6B4EE6);
Color kBlueColor = Color(0XFF2BBEF2);
Color kRoseColor = Color(0XFFEA37A3);
Color kGreenColor = Color(0XFF00CF86);
Color kBackgroundColor = Color (0XFF2b2c5d);

LinearGradient kGradient = LinearGradient(colors: [
  Color(0XFF7565ff),
  Color(0XFFac63ff),
]);

//---------new

Color kDarkBlueColor = Color (0XFF2b2c5d);
Color kLightBlueColor = Color (0XFF444673);
Color kBorderColor = Color (0XFF8b90a5).withOpacity(0.1);
Color kDisabledTextColor = Color (0XFFcccccc);
Color kEnabledTextColor = Colors.white;
Color kDisabledButtonColor = Color (0XFF8b90a5);

LinearGradient kGradientBlue = LinearGradient(colors: [
  Color(0XFF7265fe),
  Color(0XFFaa64ff),
]);

LinearGradient kGradientRose = LinearGradient(colors: [
  Color(0XFFf5696c),
  Color(0XFFfe42c9),
]);

BoxShadow kButtonShadow= BoxShadow(color:Color(0XFF7265fe).withOpacity(0.4) ,blurRadius: 8, offset: Offset(0, 4));
BoxShadow kDisabledButtonShadow= BoxShadow(color:Color(0XFF8b90a5).withOpacity(0.4) ,blurRadius: 8, offset: Offset(0, 4));




