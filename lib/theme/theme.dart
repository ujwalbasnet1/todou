import 'package:flutter/material.dart';

import 'colors.dart';

ThemeData kDatingTheme = ThemeData.light().copyWith(
  textTheme: TextTheme(
    display1: TextStyle(),
    display2: TextStyle(),
    display3: TextStyle(
      fontFamily: "Lato",
      fontSize: 32,
      fontWeight: FontWeight.bold,
    ),
    display4: TextStyle(
      fontFamily: "Lato",
      fontSize: 24,
      fontWeight: FontWeight.bold,
    ),
    headline: TextStyle(),
    subtitle: TextStyle(fontFamily: "Lato", fontSize: 16),
    subhead: TextStyle(fontFamily: "Lato", fontSize: 8),
    body1: TextStyle(fontFamily: "Lato", fontSize: 14),
    body2: TextStyle(),
    button: TextStyle(),
    caption: TextStyle(fontFamily: "Lato", fontSize: 12),
    overline: TextStyle(fontFamily: "Lato", fontSize: 10),
    title: TextStyle(fontFamily: "Lato", fontSize: 18),
  ),
  colorScheme: ColorScheme.dark(
    background: kDarkBlueColor,
    primary: kPurpleColor,
    secondary: kRoseColor,
    onPrimary: Colors.white,
  ),
  scaffoldBackgroundColor: kDarkBlueColor,
  inputDecorationTheme: InputDecorationTheme(
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(6),
      borderSide: BorderSide(
        color: kBorderColor,
      ),
    ),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(6),
      borderSide: BorderSide(
        color: kBorderColor,
      ),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(6),
      borderSide: BorderSide(
        color: kPurpleColor,
      ),
    ),
    contentPadding: EdgeInsets.symmetric(
      vertical: 12,
      horizontal: 16,
    ),
    filled: true,
    fillColor: kLightBlueColor,
  ),
  iconTheme: IconThemeData(
    color: Colors.white,
  ),
  buttonTheme: ButtonThemeData(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(6),
    ),
    textTheme: ButtonTextTheme.primary,
    disabledColor: kDisabledButtonColor,
  ),
  buttonBarTheme: ButtonBarThemeData(
    layoutBehavior: ButtonBarLayoutBehavior.constrained,
  ),
  appBarTheme: AppBarTheme(
    color: Colors.transparent,
    elevation: 0,
    iconTheme: IconThemeData(color: kMirageColor),
  ),
);
